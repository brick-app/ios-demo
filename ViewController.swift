//
//  ViewController.swift
//  Demo Brick
//
//  Created by vanbugkring on 6/4/21.
//

import UIKit
import BrickSDK


class ViewController: UIViewController {
    let clientId: String = "3424431a-7282-405c-80fd-ab3350d37370"
    let clientSecret: String = "0lfByqbHqsUOxXEVDK76XQdHttL8KB"
    let name: String = "BRICK"
    let url: String = "https://onebrick.io"
    var institutionData:InstitutionData? = nil
    @IBOutlet weak var authenticationButton: UIButton!
    @IBOutlet weak var authenticationRequestButton: UIButton!
    @IBOutlet weak var institutionListButton: UIButton!
    var brickSDK: CoreBrick? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSDK()
    }
    
    @IBAction func bankAuthentication(_ sender: Any) {
        let userParams = InstituionAuthPayload(username: "johndoe", password: "fintechFirst")
        let institutionData = InstitutionData(id: 7, bankName: "Mock Bank", bankCode: "ACMEIDJA", countryName: nil, type: "Internet Banking")
        brickSDK?.authenticateBankAccount(parameters: userParams, institutionData: institutionData, success: { result in
            guard let data = result else {
                return
            }
            print(data)
        }, failure: { error in
            print(error)
        })
    }
    @IBAction func institutionListButtonDidTapped(_ sender: Any) {
        brickSDK?.institutionListRequest(success: { result in
            guard let data = result else {
                return
            }
            self.institutionData = data.data?.first
            print(data)
        }, failure: { err in
            print(err)
        })
    }
    @IBAction func authenticationRequestButtonDidTapped(_ sender: Any) {
        brickSDK?.requestAccessTokenRequest(success: { result in
            guard let data = result else {
                return
            }
            print(data)
        }, failure: { err in
            print(err)
        })
    }
    
    @IBAction func authenticationButtonDidTapped(_ sender: Any) {
        brickSDK?.requestAccessToken(success: { result in
            guard let data = result else {
                return
            }
            print(data)
        }, failure: { err in
            print(err)
        })
    }
    
    func initSDK() -> Void {
        brickSDK = CoreBrick(clientId: clientId,
                             secret: clientSecret,
                             name: name,
                             url: url,
                             environment: Environment.SANDBOX)
        
        brickSDK?.setVerboseLogging(verbose: true)
        

    }


}

